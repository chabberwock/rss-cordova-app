// start angular app when cordova app is ready
document.addEventListener("deviceready", function() {
    var domElement = document.getElementById('angular-app');
    angular.bootstrap(domElement, ["app.rss"]);
}, false);
