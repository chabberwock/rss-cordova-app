(function() {

    angular.module('app.rss')
        .controller('ListController', controller);

    controller.$inject = ['$scope', 'RssService'];
    function controller($scope, rssService) {
        let vm = this;
        vm.url = '';
        vm.items = [];
        vm.lastInvalidUrl = 'invalid';
        vm.submit = submit;
        vm.isValidUrl = isValidUrl;
        vm.navigate = navigate;

        function submit() {
            vm.items = [];
            let url = vm.url;
            rssService.get(url)
                .then(items => {
                    vm.items = items;
                }, err => {
                    vm.lastInvalidUrl = url;
                });
        }

        function isValidUrl() {
            try {
                new URL(vm.url);
            } catch (e) {
                return false;
            }
            return vm.url !== vm.lastInvalidUrl;
        }

        function navigate(link) {
            cordova.InAppBrowser.open(link, '_system', 'location=yes');
        }

    }
})()
