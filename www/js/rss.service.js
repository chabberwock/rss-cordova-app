angular.module('app.rss')
    .factory('RssService',serviceFactory);

serviceFactory.$inject = ['$http', '$sce'];
function serviceFactory($http, $sce) {
    const apiUrl = 'https://rss-backend-cltog.ondigitalocean.app/';
    function get(url) {
        return $http.get(apiUrl, {params: {url: url}})
            .then(resp => {
                resp.data.items.forEach(item => {
                    item.description = $sce.trustAsHtml(item.description);
                });
                return resp.data.items;
            });
    }

    return {
        get: get
    }
}

