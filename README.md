# Simple RSS reader built with Apache Cordova

## About

This app displays RSS Feed loaded via API gateway: https://rss-backend-cltog.ondigitalocean.app/

URL can be modified in `/www/js/rss.service.js`



## Installation

* Install Cordova CLI: https://cordova.apache.org/docs/en/10.x/guide/cli/index.html
* Launch App `$ cordova run android`

